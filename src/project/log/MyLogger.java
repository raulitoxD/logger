package project.log;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class MyLogger {

	private ILogger[] iloggerArray;
	

	private Map<Level, Boolean> logLevelConfig = new HashMap<Level, Boolean>();

	public MyLogger(ILogger[] loggers, boolean canLogInfo, boolean canLogWarning, boolean canLogError) {
		iloggerArray = loggers;

		logLevelConfig.put(Level.WARNING, canLogWarning);
		logLevelConfig.put(Level.INFO, canLogInfo);
		logLevelConfig.put(Level.SEVERE, canLogError);

	}

	public void Log(String message, Level level) {

		for (ILogger iLogger : iloggerArray) {

			Boolean canLogThis = logLevelConfig.get(level);
			if (canLogThis) {

				iLogger.log(message, level);
			}

		}

	}


}
