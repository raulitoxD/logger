package project.log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileLoggerImp implements ILogger {
	
	private Logger logger;
	
	public FileLoggerImp(String propertyPath) {
		Properties properties = new Properties(); 
		FileInputStream fis = null;
		try {
		fis = new FileInputStream(propertyPath); 
		
		properties.load(fis);
		
		String fileName = properties.getProperty("logFileFolder") + "LogFile"+DateFormat.getDateInstance(DateFormat.LONG).format(new
				Date()) 
				 + ".txt";
		
		File file = new File(fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
 
		FileHandler fh = new FileHandler(fileName);
		logger = Logger.getLogger("MyLog");
        logger.addHandler(fh);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	 public void log(String message, Level logSeverity)
     {
         logger.log(logSeverity, DateFormat.getDateInstance(DateFormat.LONG).format(new
Date()) + message);
     }
 }
	


