package project.log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;

public class DataBaseLoggerImp implements ILogger {

	private Connection connection;
	
	public DataBaseLoggerImp() throws Exception {
		Properties connectionProps = new Properties();
	
		String url = "jdbc:" + connectionProps.getProperty("dbms") + "://" + connectionProps.getProperty("serverName") +	":" + connectionProps.getProperty("portNumber") + "/";
		connection = DriverManager.getConnection(url, connectionProps);
	}

	@Override
	public void log(String message, Level logSeverity) {
		try {
			connection.createStatement().executeUpdate("insert into Log_Values('" + DateFormat.getDateInstance(DateFormat.LONG).format(new
					Date()) + message + "', " + logSeverity.getName() + ")");
		} catch (Exception ex) {
			throw new RuntimeException(ex.getMessage(), ex);
		}
	}
	
	
	
}
