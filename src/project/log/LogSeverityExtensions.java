package project.log;


public    class LogSeverityExtensions
{
    public static boolean isInfo( LogSeverity level)
    {
        return level == LogSeverity.Info;
    }

    public static boolean isWarning( LogSeverity level)
    {
        return level == LogSeverity.Warning;
    }

    public static boolean isError( LogSeverity level)
    {
        return level == LogSeverity.Error;
    }

    public static boolean isValid( LogSeverity level)
    {
        return LogSeverityExtensions.isError(level) || LogSeverityExtensions.isInfo(level) || LogSeverityExtensions.isWarning(level);
    }
}
