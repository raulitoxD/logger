package project.log;

import java.io.FileNotFoundException;
import java.util.logging.Level;

import org.junit.Assert;
import org.junit.Test;

public class MyLoggerTest {
	
	@Test
	public void testLog() {
		
		String msg ="aaa"; 
		Level severity = Level.WARNING;
		
		ILogger logger = new ILogger() {
			
			@Override
			public void log(String message, Level logSeverity) {
				// TODO Auto-generated method stub
				Assert.assertSame(msg, message);
				Assert.assertSame(severity, logSeverity);
			}
		};
		MyLogger myLogger = new MyLogger(new ILogger[]{logger}, true, true, true);
		myLogger.Log(msg, severity);
		
	}
	
	@Test
	public void testConsoleLog() {
		
		String msg ="aaa"; 
		Level severity = Level.WARNING;
		
		ILogger logger = new ConsoleLoggerImp();
		MyLogger myLogger = new MyLogger(new ILogger[]{logger}, true, true, true);
		myLogger.Log(msg, severity);
		
	}
	
	@Test
	public void testFileLog() throws Exception {
		
		String msg ="aaa444"; 
		Level severity = Level.WARNING;
		
		ILogger logger = new FileLoggerImp("C:\\Users\\rcaja\\workspace\\Logger\\src\\project\\log\\LoggerProperties");
		MyLogger myLogger = new MyLogger(new ILogger[]{logger}, true, true, true);
		myLogger.Log(msg, severity);
		
	}

	
	@Test(expected=NullPointerException.class)
	public void testFileLogException() throws Exception {
		
		String msg ="aaa444"; 
		Level severity = Level.WARNING;
		
		ILogger logger = new FileLoggerImp("C:\\Users\\rcaja\\workspace\\Logger\\src\\project\\log\\LoggerPrperties");
		MyLogger myLogger = new MyLogger(new ILogger[]{logger}, true, true, true);
		myLogger.Log(msg, severity);
		
	}
	

}
