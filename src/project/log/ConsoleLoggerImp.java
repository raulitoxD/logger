package project.log;

import java.text.DateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsoleLoggerImp implements ILogger {
	private Logger logger;

	public ConsoleLoggerImp() {
		// TODO Auto-generated constructor stub
		ConsoleHandler ch = new ConsoleHandler();
		logger = Logger.getLogger("MyLog");
		logger.addHandler(ch);
	}
	
	 public void log(String message, Level logSeverity)
     {
         logger.log(logSeverity, DateFormat.getDateInstance(DateFormat.LONG).format(new
Date()) + message);
     }
}
